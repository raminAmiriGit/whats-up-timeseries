import numpy as np
from tensorflow import keras

def mlp_builder(layer_sizes, bias_initializer, batch_norm=None, use_residuals=False):
    """ Creates a feedforward MLP.
    
    We use linear activation at the output and ReLU at hidden layers.
    By default we create a vanilla MLP, but setting the corresponding parameters
    will add batch normalization and/or residual connections to facilitate training
    of deeper nets.
    
    Args:
        layer_sizes (int list): Number of neurons in each layer.
            Note that the first element on this list gives the input size and
            the last element gives the ouput size.
        bias_initializer (string): tf.keras initializer
        batch_norm (string): None (no batch normalization layer),
            "pre" (recommended, before the dense layer),
            "before" (batch normalization applied before the activation),
            "after" (applied after activation)
        use_residuals (bool): Whether to use skip connections.
            Note that skip connections are only used if the input and output dimensions
            of a given hidden layers are the same.
            If not they are simply omitted.
        
    Returns:
        A keras model.
    """
    assert len(layer_sizes)>1, (
        "Network must contain at least an input and output layer")
    assert batch_norm in [None, 'pre', 'before', 'after'], (
        "batch_normalization has incorrect value")
    
    # input layer
    pix_loc = keras.layers.Input(shape=(layer_sizes[0],))
    
    # hidden layers
    layer_input = pix_loc
    for this_width in layer_sizes[1:-1]:
        # place the batch normalization layer
        if batch_norm == 'pre':
            y = keras.layers.BatchNormalization()(layer_input)
        else:
            y = layer_input
            
        y = keras.layers.Dense(this_width, activation=None)(y)
        
        # place the batch normalization layer
        if batch_norm == 'before':
            y = keras.layers.BatchNormalization()(y)
        y = keras.layers.Activation("relu")(y)
        if batch_norm == 'after':
            y = keras.layers.BatchNormalization()(y)
            
        # place the residual connection
        # residual connection is identity -> 
        # can only work between input and output of same dimension
        if use_residuals and (layer_input.shape[1] == this_width):
            layer_input = keras.layers.Add()([layer_input, y])
        else:
            layer_input = y

    # output layer
    color = keras.layers.Dense(layer_sizes[-1], activation=None)(layer_input)

    return keras.models.Model(pix_loc, color)


def viz_history(history, figsize=(9, 9/1.6)):
    """ Visualizes training history.
    
    Args:
       history (dict): Each key should map to a 1D numpy array
           which length is the number of epochs.
       figsize (tuple): Size of the figure.
       
    """
    import matplotlib.pyplot as plt

    _, (ax1, ax2) = plt.subplots(2, 1, sharex=True, figsize=figsize)
    ax1.plot(history['lr'])
    ax1.set_title('Learning rate schedule')
    ax1.set_ylabel('learning rate')
    ax2.plot(history['loss'])
    ax2.set_title('Loss')
    ax2.set_xlabel('epochs')
    ax2.set_ylabel('loss')
    plt.show()
    
    
def pic_to_long(pic_array):
    """ Converts a picture array to long format.
    
    Args:
        pic_array (np 3D): Picture array of shape (height, width, 3) for RGB pictures.
            The function supports an arbitrary number of channels.
            That is pic_array can also be an array of shape (height, width, channels).
    
    Returns:
        Tuple of arrays:
            1. pixel location array of shape (n_pixels, 2).
                Such that pix_loc[i] = [height of pixel #i, width of pixel #i].
            2. pixel value array of shape (n_pixels, 3).
                Such that pic_long[i] = [red value of pix #i, green of #i, blue of #i]
                
        These 2 arrays are related by the following equality:
            pic_array[pix_loc[i]] = pic_long[i]
    """
    # basic image dimensions
    height, width, channels = pic_array.shape
    n_pixels = height*width
    # create pixel values array
    pic_long = pic_array.reshape(-1, channels)
    
    # create pixel location array
    # pix_y[x, y] = y (y coordinate tensor)
    pix_y = np.repeat(np.array(range(height))[:, np.newaxis], width, axis=1)
    # pix_x[x, y] = x
    pix_x = np.repeat(np.array(range(width))[np.newaxis, :], height, axis=0)
    # pix_loc[x, y] = [x, y]
    pix_loc = np.dstack([pix_y, pix_x])
    pix_loc_long = pix_loc.reshape(-1, 2)
    
    return pix_loc_long, pic_long