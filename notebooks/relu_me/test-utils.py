""" Testing utils.
"""

import utils
import numpy as np

def test_pic_to_long():
    """ Tests pic_to_long on dummy data. """
    test_pic = np.random.normal(size=(20, 30, 3))
    pix_loc_long, pic_long = utils.pic_to_long(test_pic)
    assert np.all(pix_loc_long[:, 0] < 20),\
        "Height of pixels cannot be above image height"
    assert np.all(pix_loc_long[:, 1] < 30),\
        "Width of pixel cannot be above image width"
    np.testing.assert_array_equal(
        test_pic[pix_loc_long[:, 0], pix_loc_long[:, 1]],
        pic_long,
        err_msg="Error in indexing")

    
def test_mlp_builder():
    """ Tests mlp_builder in linear case. """
    test_model = utils.mlp_builder([2, 1], "zeros")
    # compute total number of parameters
    n_weights = 0
    for this_layer in test_model.layers:
        for this_weight in this_layer.get_weights():
            n_weights += np.size(this_weight)
    assert n_weights == 3, "Expected 2 coefficients + 1 bias = 3 parameters"
    
    

def test_mlp_builder2():
    """ Tests mlp_builder residuals connections. """
    test_model = utils.mlp_builder([2, 2, 2, 1], "zeros")
    test_model_resid = utils.mlp_builder([2, 2, 2, 1], "zeros", use_residuals=True)
    # compute total number of parameters in first model
    n_weights = 0
    for this_layer in test_model.layers:
        for this_weight in this_layer.get_weights():
            n_weights += np.size(this_weight)
    # compute total number of parameters in second model
    n_weights_resid = 0
    for this_layer in test_model_resid.layers:
        for this_weight in this_layer.get_weights():
            n_weights_resid += np.size(this_weight)
    assert n_weights == n_weights_resid, "Residual connections should not add weights"