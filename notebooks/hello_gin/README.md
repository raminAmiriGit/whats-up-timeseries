# Gin Hello World

This is the simplest example for using gin.
We define a function in utils.py, we configure its parameters in config.gin, finally in the hello-gin notebook we call the function without having to specify the parameters.


In practice, the function could be a training function containing a lot of ML parameters.
And the config file would be a convenient way to set those parameters.