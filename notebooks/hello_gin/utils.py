""" utils.py

In a real project this module would contain your model code.
"""

import gin

@gin.configurable
def my_function(a, b, c):
    return a, b, c